#!/bin/sh

# Set HOME directory
HOME="${HOME:-$(getent passwd $USER 2>/dev/null | cut -d: -f6)}"
# macOS does not have getent, but this works even if $HOME is unset
HOME="${HOME:-$(eval echo ~$USER)}"

# Define repository URL
REPO_URL="https://gitlab.com/Kurisunyanyan/vtol_update.git"
CLONE_DIR="$HOME/vtol_update"
NX_ROS_REPO_DIR="$HOME/nx_ros_repo_test"
CURRENT_TIME=$(date +%Y%m%d_%H%M%S)

# Clone the repository
echo "Cloning repository from $REPO_URL to $CLONE_DIR..."
git clone $REPO_URL $CLONE_DIR

# Move build.zip, devel.zip, and src.zip to nx_ros_repo
echo "Moving build.zip, devel.zip, and src.zip to $NX_ROS_REPO_DIR..."
mv $CLONE_DIR/build.zip $NX_ROS_REPO_DIR
mv $CLONE_DIR/devel.zip $NX_ROS_REPO_DIR
mv $CLONE_DIR/src.zip $NX_ROS_REPO_DIR

# Rename existing build and devel folders with current time
if [ -d "$NX_ROS_REPO_DIR/build" ]; then
    echo "Renaming existing build folder to build_$CURRENT_TIME..."
    mv $NX_ROS_REPO_DIR/build $NX_ROS_REPO_DIR/build_$CURRENT_TIME
fi

if [ -d "$NX_ROS_REPO_DIR/devel" ]; then
    echo "Renaming existing devel folder to devel_$CURRENT_TIME..."
    mv $NX_ROS_REPO_DIR/devel $NX_ROS_REPO_DIR/devel_$CURRENT_TIME
fi

# Delete existing src folder
if [ -d "$NX_ROS_REPO_DIR/src" ]; then
    echo "Deleting existing src folder..."
    rm -rf $NX_ROS_REPO_DIR/src
fi

# Unzip build.zip, devel.zip, and src.zip
echo "Unzipping build.zip..."
unzip $NX_ROS_REPO_DIR/build.zip -d $NX_ROS_REPO_DIR
echo "Unzipping devel.zip..."
unzip $NX_ROS_REPO_DIR/devel.zip -d $NX_ROS_REPO_DIR
echo "Unzipping src.zip..."
unzip $NX_ROS_REPO_DIR/src.zip -d $NX_ROS_REPO_DIR

# Move launch_script.py to the root directory
echo "Moving launch_script.py to $HOME..."
mv $CLONE_DIR/launch_script.py $HOME

# Clean up
echo "Cleaning up..."
rm $NX_ROS_REPO_DIR/build.zip
rm $NX_ROS_REPO_DIR/devel.zip
rm $NX_ROS_REPO_DIR/src.zip
rm -rf $CLONE_DIR

echo "Update completed."
