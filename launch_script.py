import argparse
import subprocess

def run_command(command):
    try:
        subprocess.run(command, check=True, shell=True)
    except subprocess.CalledProcessError as e:
        print(f"Error executing command: {e}")

# Parse arguments
parser = argparse.ArgumentParser(description="Drone configuration and launch script.")

parser.add_argument("-r", "--record_rosbag", action="store_true", help="Record ROS bag")
parser.add_argument("-d", "--start_mission", action="store_true", help="Enable MicroXRCEAgent")
parser.add_argument("-n", "--drone_namespace", type=str, default="drone", help="Drone namespace")
parser.add_argument("-id", "--uav_id", type=int, default=1, help="UAV ID")
parser.add_argument("-max_aspd", "--max_airspeed", type=float, default=35.0, help="Max airspeed")
parser.add_argument("-min_aspd", "--min_airspeed", type=float, default=26.0, help="Min airspeed")
parser.add_argument("-acc_r", "--accept_radius", type=float, default=300.0, help="Accept radius")
parser.add_argument("-l_time", "--loiter_time", type=float, default=300.0, help="Loiter duration time")
parser.add_argument("-e_hdg", "--heading_error", type=float, default=10.0, help="Assemble heading error")
parser.add_argument("-c_lost", "--communication_lost", type=float, default=5.0, help="Max communication lost")
parser.add_argument("-s_log", "--save_log", type=int, default=0, help="Save control frame log")
parser.add_argument("-d_en", "--debug_info_enable", type=int, default=0, help="Enable debug info")

args = parser.parse_args()

# Print all settings
print("Configuration Settings:")
print(f"Mission: {args.start_mission}")

if args.start_mission:
    print("Starting Mission")
    
    # Generate the namespace based on the drone_namespace and uav_id
    ns = f"{args.drone_namespace}_{args.uav_id - 1}"
    print(f"Drone Namespace: {ns}")
    launch_command_1 = (
        f"roslaunch launch_config communication_all_serial.launch "
        f"ns:={ns} aspd_max:={args.max_airspeed} aspd_min:={args.min_airspeed} "
        f"dl_p:=/dev/ttyUSB0 dl_b:=115200 fcu_p:=/dev/ttyUSB1 fcu_b:=115200 "
        f"w_fc:=1 ex_fc:=0 pub_s:=1 pub_m:=1 debug_en:={args.debug_info_enable} save_log:={args.save_log}"
    )
    launch_command_2 = (
        f"sleep 3 && roslaunch launch_config flight_control_and_navigation.launch "
        f"h:=30 e_h:=5 mode:=1 h_t:=25 alt_foh:=0 rec_en:=0 g_en:=0 "
        f"id:={args.uav_id} ns:={ns} aspd_max:={args.max_airspeed} aspd_min:={args.min_airspeed} "
        f"acc_r:={args.accept_radius} l_time:={args.loiter_time} comm_lost:={args.communication_lost} "
        f"a_hdg_err:={args.heading_error}"
    )
    


    command = (
        f"tmux new-session -d -s {ns} -n 'platform' && "
        f"tmux send-keys -t {ns}:0 '{launch_command_1}' C-m && "
        f"tmux new-window -t {ns} -n 'control' && "
        f"tmux send-keys -t {ns}:1 '{launch_command_2}' C-m"
    )

    print(f"Running command: {command}")
    run_command(command)

    # Attach to tmux session
    run_command(f"tmux attach-session -t {ns}")
